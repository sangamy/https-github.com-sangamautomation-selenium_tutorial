# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Selenium Automation Practice Tutorial Project ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

GitHub Configuration:
 
 1. Create account in github (https://github.com)
 2. Goto  https://github.com/sangamy/selenium-tutorial.git
 3. Fork it (so that we don't affect the master with our changes)
 4. Clone or Download : Clone your fork into our local system by open in desktop option/ Download Zip
 5. UnZip the project
 6. Import project into your Eclipse
 7. They will make some changes (Homework)
 8. Install GitHub using Help->Install New Software/Marketplace
 9. Edit/Add new files
 10. Add to Index : For New files only
 11. Edit Files: Team->Synchronize->Commit->Commit and Push
 12. Add Menu Item for Git [Window-Perspective-CustomizePerspective]: Commit directly & mention Commit comment & Git Staging: Commit and Push
